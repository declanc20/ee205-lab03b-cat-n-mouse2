#!/bin/bash

DEFAULT_MAX_NUMBER=2048

THE_MAX_VALUE=${1:-$DEFAULT_MAX_NUMBER}

#echo The max value is $THE_MAX_VALUE

THE_NUMBER_IM_THINKING_OF=$((RANDOM%$THE_MAX_VALUE+1))

#echo The number I\'m thinking of is $THE_NUMBER_IM_THINKING_OF
 input=0 #initilaize input to a number that the number im thinking of will never be so can get in the loop

 while [ "$input" -ne "$THE_NUMBER_IM_THINKING_OF" ] 
do
echo OK cat, I\'m thinking of a number from 1 to $THE_MAX_VALUE. Make a Guess:
read input

if [ "$input" -lt 1 ]
then echo "You must enter a number that's >= one"


elif [ "$input" -gt "$THE_MAX_VALUE" ]
then echo "You must enter a number that's <= $THE_MAX_VALUE"


elif [ "$input" -gt $THE_NUMBER_IM_THINKING_OF ]
then echo "No cat... the number I'm thinking of is smaller than $input"

elif [ "$input" -lt $THE_NUMBER_IM_THINKING_OF ]
then echo  "No cat... the number I'm thinking of is larger than $input"

elif [ "$input" -eq $THE_NUMBER_IM_THINKING_OF ]
then echo  "You got me"
echo " /\_/\\"
echo "( o.o )"
echo " > ^ <"
fi
done
